﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            myHomeVideo.Visibility = Visibility.Visible;
            
            myHomeVideo.Play();
        }

        UserAge ua;
        ItemDetail itemdts;
        Listing lst;
        void ua_myEventDOB(object sender, EventArgs e)
        {   
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        void lst_myeventItemDetailsWM(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            wm = new WhyteMacan();
            ParentPanel.Children.Add(wm);
            wm.myEventWM += wm_myEventWM;
        }

        void wm_myEventWM(object sender, EventArgs e)
        {

            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM+=lst_myeventItemDetailsWM;
        }

        ItemJura itmJura;
        ItemDetDalmore itemDalmore;
        WhyteMacan wm;
        void lst_myeventItemDetailsDalmore(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itemDalmore = new ItemDetDalmore();
            ParentPanel.Children.Add(itemDalmore);
            itemDalmore.myEventDalmoreFlip += itemDalmore_myEventDalmoreFlip;
            
        }

        void itemDalmore_myEventDalmoreFlip(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura+=lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

    
        CreateDrink cDrink;
        void lst_myeventItemDetails(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itemdts = new ItemDetail();
            ParentPanel.Children.Add(itemdts);
            itemdts.myEventFlip += itemdts_myEventFlip;

            itemDalmore = new ItemDetDalmore();
            itemDalmore.myEventDalmoreFlip+=itemDalmore_myEventDalmoreFlip;

            itmJura = new ItemJura();
            itmJura.myEventJuraFlip += itmJura_myEventJuraFlip;

            wm = new WhyteMacan();
            wm.myEventWM+=wm_myEventWM;
        }

        void itmJura_myEventJuraFlip(object sender, EventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);

            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        void lst_myeventItemDetaisJura(object sender, EventArgs e)
        {

            headerButtonPanel.Visibility = Visibility.Visible;
            ParentPanel.Children.Clear();
            itmJura = new ItemJura();
            ParentPanel.Children.Add(itmJura);
            itmJura.myEventJuraFlip+=itmJura_myEventJuraFlip;
        }

        void itemdts_myEventFlip(object sender, EventArgs e)
        {
            ParentPanel.Children.Clear();
            lst = new Listing();
            ParentPanel.Children.Add(lst);
            lst.myeventItemDetails += lst_myeventItemDetails;
            lst.myeventItemDetailsDalmore += lst_myeventItemDetailsDalmore;
            lst.myeventItemDetaisJura += lst_myeventItemDetaisJura;
            lst.myeventItemDetailsWM += lst_myeventItemDetailsWM;
        }

        private void btnTouch_TouchDown(object sender, TouchEventArgs e)
        {
            btnTouch.Visibility = Visibility.Hidden;
            myHomeVideo.Visibility = Visibility.Hidden;
            
            myHomeVideo.Stop();
            myHomeVideo.Volume = 0;
            
           
            ua = new UserAge();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(ua);
            ua.myEventDOB += ua_myEventDOB;

         }

        

        private void btnHome_TouchDown(object sender, TouchEventArgs e)
        {
            headerButtonPanel.Visibility = Visibility.Hidden;
            btnTouch.Visibility = Visibility.Visible;
            myHomeVideo.Visibility = Visibility.Visible;
            myHomeVideo.Play();
            myHomeVideo.Volume = 90;
            ParentPanel.Children.Clear();
           
        }
        UserShareLink userlnk;
        MyDrink myDrnk;
        private void btnCreateADrink_TouchDown_1(object sender, TouchEventArgs e)
        {
            ParentPanel.Children.Clear();
            cDrink = new CreateDrink();
            ParentPanel.Children.Add(cDrink);
        }

        private void btnMyDrink_TouchDown_1(object sender, TouchEventArgs e)
        {
            myDrnk = new MyDrink();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(myDrnk);
            myDrnk.myDrinkEventHandler += myDrnk_myDrinkEventHandler;  
        }
        void myDrnk_myDrinkEventHandler(object sender, EventArgs e)
        {
            userlnk = new UserShareLink();
            ParentPanel.Children.Clear();
            ParentPanel.Children.Add(userlnk);
        }

    }
}

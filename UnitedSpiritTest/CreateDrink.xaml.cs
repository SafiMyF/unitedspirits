﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UnitedSpiritTest
{
    /// <summary>
    /// Interaction logic for CreateDrink.xaml
    /// </summary>
    public partial class CreateDrink : UserControl
    {
        public CreateDrink()
        {
            InitializeComponent();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get TabControl reference.
            var item = sender as TabControl;
            // ... Set Title to selected tab header.
            var selected = item.SelectedItem as TabItem;
        
            //this.Title = selected.Header.ToString();
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void lnkNameYourDrink_TouchDown_1(object sender, TouchEventArgs e)
        {
            txtDrinkName.Visibility = Visibility.Visible;
            txtDrinkName1.Visibility = Visibility.Visible;
            txtDrinkName2.Visibility = Visibility.Visible;
        }

        private void txtDrinkName_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            txtDrinkName.Text = txtDrinkName.Text;
            txtDrinkName1.Text = txtDrinkName.Text;
            txtDrinkName2.Text = txtDrinkName.Text;
        }

        private void txtDrinkName1_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            txtDrinkName.Text = txtDrinkName1.Text;
            txtDrinkName1.Text = txtDrinkName1.Text;
            txtDrinkName2.Text = txtDrinkName1.Text;
        }

        private void txtDrinkName2_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            txtDrinkName.Text = txtDrinkName2.Text;
            txtDrinkName1.Text = txtDrinkName2.Text;
            txtDrinkName2.Text = txtDrinkName2.Text;
        }
    }
}
